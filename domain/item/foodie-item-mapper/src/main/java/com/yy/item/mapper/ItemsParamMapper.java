package com.yy.item.mapper;


import com.yy.item.pojo.ItemsParam;
import com.yy.my.mapper.MyMapper;

public interface ItemsParamMapper extends MyMapper<ItemsParam> {
}