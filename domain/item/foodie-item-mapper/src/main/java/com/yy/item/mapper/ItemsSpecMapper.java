package com.yy.item.mapper;


import com.yy.item.pojo.ItemsSpec;
import com.yy.my.mapper.MyMapper;

public interface ItemsSpecMapper extends MyMapper<ItemsSpec> {
}