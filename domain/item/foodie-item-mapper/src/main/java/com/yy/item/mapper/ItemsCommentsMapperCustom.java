package com.yy.item.mapper;

import com.yy.item.pojo.ItemsComments;
import com.yy.item.pojo.vo.MyCommentVO;
import com.yy.my.mapper.MyMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ItemsCommentsMapperCustom extends MyMapper<ItemsComments> {

    public void saveComments(Map<String, Object> map);

    public List<MyCommentVO> queryMyComments(@Param("paramsMap") Map<String, Object> map);

}