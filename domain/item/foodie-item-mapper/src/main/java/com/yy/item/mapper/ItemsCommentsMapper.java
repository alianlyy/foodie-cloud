package com.yy.item.mapper;


import com.yy.item.pojo.ItemsComments;
import com.yy.my.mapper.MyMapper;

public interface ItemsCommentsMapper extends MyMapper<ItemsComments> {
}