package com.yy.item.mapper;

import com.yy.item.pojo.ItemsImg;
import com.yy.my.mapper.MyMapper;

public interface ItemsImgMapper extends MyMapper<ItemsImg> {
}