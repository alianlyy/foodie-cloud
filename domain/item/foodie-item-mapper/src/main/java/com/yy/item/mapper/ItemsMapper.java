package com.yy.item.mapper;


import com.yy.item.pojo.Items;
import com.yy.my.mapper.MyMapper;

public interface ItemsMapper extends MyMapper<Items> {
}